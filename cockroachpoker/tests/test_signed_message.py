from unittest import TestCase
from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Hash import MD5

from cockroachpoker.signed_message import sign, verify, BadSignatureError

class WithKey(TestCase):
    def setUp(self):
        self.key = RSA.generate(2048, Random.new().read)
        self.public_key = self.key.publickey()


class TestSign(WithKey):
    def test_message_structure(self):
        msg = "Mary had a little lamb, it's fleece was white as snow"
        signed_msg = sign('alex', self.key, msg)
        self.assertEqual('alex', signed_msg['from'])
        self.assertEqual(msg, signed_msg['message'])
        self.assertTrue('signature' in signed_msg)


class TestVerify(WithKey):
    def test_success(self):
        msg = "Mary had a little lamb, it's fleece was white as snow"
        signed_msg = sign('alex', self.key, msg)
        verify(self.public_key, signed_msg)

    def test_failure_wrong_key(self):
        msg = "Mary had a little lamb, it's fleece was white as snow"
        signed_msg = sign('alex', self.key, msg)
        another_key = RSA.generate(2048, Random.new().read).publickey()
        with self.assertRaises(BadSignatureError):
            verify(another_key, signed_msg)

    def test_failure_tampered_message(self):
        msg = "Mary had a little lamb, it's fleece was white as snow"
        signed_msg = sign('alex', self.key, msg)
        signed_msg['message'] ="Mary had a little pig, and bacon is so tasty"
        with self.assertRaises(BadSignatureError):
            verify(self.public_key, signed_msg)
