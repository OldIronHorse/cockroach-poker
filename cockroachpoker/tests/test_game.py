from unittest import TestCase
from cockroachpoker.game import Game, Player, Suit, Action

class TestGameStart(TestCase):
    def test_create(self):
        game = Game()
        self.assertEqual(0, len(game.players))

    def test_join(self):
        game = Game()
        game.join('bob')
        self.assertEqual(
                {'bob': Player(name='bob')},
                game.players)

    def test_deal(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        game.join('jan')
        game.join('pam')
        game.deal()
        self.assertEqual(
                1,
                len([p for n, p in game.players.items() if len(p.hand) == 12]))
        self.assertEqual(
                4,
                len([p for n, p in game.players.items() if len(p.hand) == 13]))

    def test_first_turn(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        game.join('jan')
        game.join('pam')
        game.deal()
        self.assertEqual(Action.PLAY, game.table()['next']['action'])
        self.assertTrue(game.table()['next']['player'] in game.players.keys())

    def test_table_initial(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        game.join('jan')
        game.join('pam')
        game.deal()
        table = game.table()
        self.assertEqual(
                {'bob': {}, 'bill': {}, 'sue': {}, 'jan': {}, 'pam': {}},
                table['tabled'])
        self.assertEqual({}, table['played'])
        self.assertEqual(Action.PLAY, table['next']['action'])
        self.assertTrue(table['next']['player'] in game.players.keys())


class TestGamePlay(TestCase):
    def setUp(self):
        self.game = Game()
        self.game.join('bob')
        self.game.join('bill')
        self.game.join('sue')
        self.game.players['bob'].hand = [Suit.FLY, Suit.FLY, Suit.BAT]
        self.game.next = ('bob', Action.PLAY)

    def test_play(self):
        player_to_play, action = self.game.next_player()
        self.assertEqual('bob', player_to_play)
        self.assertEqual(Action.PLAY, action)
        self.game.play('bob', 'bill', Suit.FLY, Suit.BAT)
        self.assertEqual([Suit.FLY, Suit.BAT], self.game.players['bob'].hand)
        next_player, action = self.game.next_player()
        self.assertEqual('bill', next_player)
        self.assertEqual(Action.CALL_OR_PASS, action)
        self.assertEqual(
                {'from': ['bob'], 'to': 'bill', 'claim': Suit.BAT},
                self.game.table()['played'])


class TestGameCallPassLie(TestCase):
    def setUp(self):
        self.game = Game()
        self.game.join('bob')
        self.game.join('bill')
        self.game.join('sue')
        self.game.players['bob'].hand = [Suit.FLY, Suit.FLY, Suit.BAT]
        self.game.next = ('bob', Action.PLAY)
        self.game.play('bob', 'bill', Suit.FLY, Suit.BAT)

    def test_call_correct(self):
        result = self.game.call(False)
        self.assertTrue(result)
        self.assertEqual(('bob', Action.PLAY), self.game.next_player())
        self.assertEqual({
            'tabled': {
                'bob': {Suit.FLY: 1},
                'bill': {},
                'sue': {}
                },
            'played': {},
            'next': {'player': 'bob', 'action': Action.PLAY}
            },
            self.game.table())

    def test_call_incorrect(self):
        result = self.game.call(True)
        self.assertFalse(result)
        self.assertEqual(('bill', Action.PLAY), self.game.next_player())
        self.assertEqual({
            'tabled': {
                'bob': {},
                'bill': {Suit.FLY: 1},
                'sue': {}
                },
            'played': {},
            'next': {'player': 'bill', 'action': Action.PLAY}
            },
            self.game.table())

    def test_pass(self):
        self.assertEqual((Suit.FLY, {'sue'}), self.game.will_pass())
        self.assertEqual(('bill', Action.PASS), self.game.next_player())
        self.game.pass_on('bill', 'sue', Suit.FLY, Suit.RAT)
        self.assertEqual({
            'tabled': {
                'bob': {},
                'bill': {},
                'sue': {}
                },
            'played': {'from': ['bob', 'bill'], 'to': 'sue', 'claim': Suit.RAT},
            'next': {'player': 'sue', 'action': Action.CALL},
            },
            self.game.table())


class TestGameCallPassTruth(TestCase):
    def setUp(self):
        self.game = Game()
        self.game.join('bob')
        self.game.join('bill')
        self.game.join('sue')
        self.game.players['bob'].hand = [Suit.FLY, Suit.FLY, Suit.BAT]
        self.game.next = ('bob', Action.PLAY)
        self.game.play('bob', 'bill', Suit.FLY, Suit.FLY)

    def test_call_correct(self):
        result = self.game.call(True)
        self.assertTrue(result)
        self.assertEqual(('bob', Action.PLAY), self.game.next_player())
        self.assertEqual({
            'tabled': {
                'bob': {Suit.FLY: 1},
                'bill': {},
                'sue': {}
                },
            'played': {},
            'next': {'player': 'bob', 'action': Action.PLAY}
            },
            self.game.table())

    def test_call_incorrect(self):
        result = self.game.call(False)
        self.assertFalse(result)
        self.assertEqual(('bill', Action.PLAY), self.game.next_player())
        self.assertEqual({
            'tabled': {
                'bob': {},
                'bill': {Suit.FLY: 1},
                'sue': {}
                },
            'played': {},
            'next': {'player': 'bill', 'action': Action.PLAY}
            },
            self.game.table())

    def test_pass(self):
        self.assertEqual((Suit.FLY, {'sue'}), self.game.will_pass())
        self.assertEqual(('bill', Action.PASS), self.game.next_player())
        self.game.pass_on('bill', 'sue', Suit.FLY, Suit.RAT)
        self.assertEqual({
            'tabled': {
                'bob': {},
                'bill': {},
                'sue': {}
                },
            'played': {'from': ['bob', 'bill'], 'to': 'sue', 'claim': Suit.RAT},
            'next': {'player': 'sue', 'action': Action.CALL}
            },
            self.game.table())


class TestGameCheckLoser(TestCase):
    def test_no_loser(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        self.assertEqual(None, game.check_loser())

    def test_sue_lost(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        game.players['sue'].tabled = {Suit.FLY: 4, Suit.RAT: 3}
        game.players['bill'].tabled = {Suit.FLY: 3, Suit.BAT: 3}
        game.players['bob'].tabled = {Suit.STINKBUG: 1}
        self.assertEqual('sue', game.check_loser())


#TODO: check_loser()
