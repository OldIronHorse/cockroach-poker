from unittest import TestCase
from Crypto.PublicKey import RSA
import json

from cockroachpoker.encrypted_message import encrypt, decrypt


class TestEncryptDecrypt(TestCase):
    def setUp(self):
        self.private_key = RSA.generate(2048)
        self.public_key = self.private_key.publickey()

    def test_encrypted_message_structure(self):
        plaintext = "Larry had a little lamb. It's fleece was thick and curly."
        msg = encrypt(self.public_key, plaintext)
        self.assertTrue('ciphertext' in msg)
        self.assertTrue('session_key' in msg)
        self.assertTrue('nonce' in msg)
        self.assertTrue('tag' in msg)

    def test_encrypt_decrypt(self):
        plaintext_out = "Larry had a little lamb. It's fleece was thick and curly."
        msg_out = json.dumps(encrypt(self.public_key, plaintext_out))
        msg_in = json.loads(msg_out)
        plaintext_in = decrypt(self.private_key, msg_in)
        self.assertEqual(plaintext_out, plaintext_in)


